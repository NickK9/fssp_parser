from flask import Flask, request, jsonify
import time
import json

from models import Tokens
from database import SessionLocal, engine
import models
from fssp_token_generator import TokenGenerator


models.Base.metadata.create_all(bind=engine)

session = SessionLocal()

app = Flask(__name__)

@app.route('/api/1.0/available_amount', methods=['GET'])
def available_tokens_amount():
    '''
    Отдавать количество токенов , которые можно получить "прямо сейчас"
    '''
    available_tokens = session.query(Tokens).filter_by(is_sent=False).all()
    amount = len(available_tokens)

    return jsonify({'Tokens available': amount})

@app.route('/api/1.0/get_tokens/<int:tokens_amount>', methods=['GET'])
def get_tokens(tokens_amount):
    '''
    Отдавать токены по запросу в котором указано количество нужных токенов. Возвращать понятную ошибку если токенов меньше чем надо
    '''
    available_tokens = session.query(Tokens).filter_by(is_sent=False).all()
    if len(available_tokens) < tokens_amount:
        data = {
            'ERROR': 'Not enough tokens',
            'Available tokens': len(available_tokens),
        }
    else:
        data = {
            'Tokens': [token.token for token in available_tokens[:tokens_amount]],
            'Amount': tokens_amount,
            'Tokens left': len(available_tokens) - tokens_amount,
        }
        for token in available_tokens[:tokens_amount]: # токен был отправлен
            token.is_sent = True
        session.commit()
    return jsonify(data)

@app.route('/api/1.0/generate_tokens/<int:tokens_amount>', methods=['GET'])
def generate_tokens(tokens_amount):
    '''
    Принимать задачу на генерацию требуемого количества токенов
    '''
    if tokens_amount < 1:
        return jsonify({'ERROR': 'Not valid number'})
    with open('companies_names.txt', 'r') as f:
        companies = f.read().split('\n')
    with open('tokens.json', 'w') as f:
        json.dump([], f)
    retry = 3
    for company in companies:
        if retry == 0:
            break
        try:
            token = TokenGenerator(company, tokens_amount)
            if not token.second_browser or not token.browser:
                break
            token.get_mail()
            token.fssp_registration()
            token.get_link()
            token.get_token()
            new_token = Tokens(token=token.token, is_sent=False, phone_number=token.phone_number, email=token.temp_email, company_name=token.company, aim=token.reason_text)
            session.add(new_token)
            session.commit()
            if token.is_finished:
                break
        except Exception as e:
            print(str(e))
            retry -= 1
            time.sleep(60)
    if retry == 0:
        return jsonify({'ERROR':'Error with parsing'})
    else:
        return jsonify({'SUCCESS':'DONE'})
    

if __name__ == '__main__':
    app.debug = True 
    app.run()