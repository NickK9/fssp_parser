from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship

from database import Base


class Tokens(Base):
    __tablename__ = "tokens"

    id = Column(Integer, primary_key=True)
    token = Column(String, unique=True)
    is_sent = Column(Boolean, default=False, index=True)
    phone_number = Column(String)
    email = Column(String, unique=True)
    company_name = Column(String)
    aim = Column(String)