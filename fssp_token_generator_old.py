from selenium import webdriver
import time
import json
import re
import random


def fssp_registration(browser, temp_email, company):
    '''
    Метод позволяет вбить нужные данные на страницу регистрации ФССП
    browser: объект браузера selenium
    temp_email: str временный мейл
    company: str рандомное название компании из файла

    return: True
    '''
    phone_number = '+79029' + str(random.randint(000000, 999999))
    reason_text = random.choice(['Инфоуслуги', 'Информация', 'Получение информации', 'Информационные услуги'])
    browser.get('https://api-ip.fssprus.ru/register')
    email = browser.find_element_by_id('email')
    email.send_keys(temp_email)
    organisation_name = browser.find_element_by_id('name')
    organisation_name.send_keys(company)
    phone = browser.find_element_by_id('phone')
    phone.send_keys(phone_number)
    reason = browser.find_element_by_id('reason')
    reason.send_keys(reason_text)
    print('CAPTCHA')
    captcha = browser.find_element_by_name('captcha')
    my_captcha = input('Введи капчу: ')
    captcha.send_keys(my_captcha)
    submit_button = browser.find_element_by_xpath('/html/body/div[2]/section/div/div/div[2]/div/div/form/div[7]/div[1]/input')
    submit_button.click()
    return True

def get_mail(second_browser):
    '''
    Метод получить временный email на dropmail.me
    second_browser: объект браузера selenium (второй экземпляр)

    return: Полученный email
    '''
    second_browser.get('https://dropmail.me/ru/')
    while True: # ждем пока загрузится страница
        try:
            temp_email = second_browser.find_element_by_class_name('email').text
            break
        except:
            pass 
    return temp_email

def get_link(second_browser):
    '''
    Метод получить найти ссылку на получение токена в мейле
    second_browser: объект браузера selenium (второй экземпляр)

    return: Найденная ссылка
    '''
    while True: # ждем пока придет емейл
        try:
            confirm = second_browser.find_element_by_xpath('/html/body/div[2]/div[5]/div/ul/li/div[1]')
            break
        except:
            pass
    message_text = confirm.text
    link = re.search(r'(https://api-ip.fssprus.ru/email/confirm/[A-Za-z0-9]+)', message_text)
    link = link.group(0)
    return link

def get_token(browser, link):
    '''
    Метод получить найти токен на ФППС
    second_browser: объект браузера selenium
    link: str ссылка на подтверждния мейла, чтобы получить токен

    return: True - если определенное количество набралось, false - если еще надо парсить
    '''
    browser.get(link)
    token_text = browser.find_element_by_xpath('/html/body/div[2]/section/div/div/div[2]/div').text
    token = re.search(r'Ваш ключ доступа к API: ([A-Za-z0-9]+)', token_text)
    token = token.group(0)
    token = token.split('Ваш ключ доступа к API: ')[1]
    with open('tokens.json', 'r') as f:
        tokens = json.load(f)
    tokens.append(token)
    with open('tokens.json', 'w') as f:
        json.dump(tokens, f)
    print('Осталось', 400-len(tokens))
    if len(tokens) >= 400:
        return True
    return False

if __name__ == '__main__':
    with open('companies_names.txt', 'r') as f:
        companies = f.read().split('\n')
    for company in companies:
        try:
            second_browser = webdriver.Chrome(executable_path=r'./chromedriver')
            browser = webdriver.Chrome(executable_path=r'./chromedriver')
            current_mail = get_mail(second_browser)
            is_submited = fssp_registration(browser, current_mail, company)
            if is_submited:
                link = get_link(second_browser)
                if link:
                    time.sleep(1)
                    finish = get_token(browser, link)
                    if finish:
                        break
            browser.close()
            second_browser.close()
        except Exception as e:
            print(str(e))
            time.sleep(15)
    print('READY')
