from selenium import webdriver
import time
import json
import re
import random
import selenium.common.exceptions as selenium_exceptions
import base64
import requests


class TokenGenerator():

    def __init__(self, company, limit):
        self.company = company
        self.limit = limit
        try:
            self.second_browser = webdriver.Chrome(executable_path=r'./chromedriver.exe')
            self.browser = webdriver.Chrome(executable_path=r'./chromedriver.exe')
        except selenium_exceptions.SessionNotCreatedException:
            self.second_browser = False
            self.browser = False
            print('Обновите Chrome и ChromeDriver')

    def get_mail(self):
        '''
        Метод получить временный email на dropmail.me
        second_browser: объект браузера selenium (второй экземпляр)

        return: Полученный email
        '''
        self.second_browser.get('https://dropmail.me/ru/')
        while True: # ждем пока загрузится страница
            try:
                temp_email = self.second_browser.find_element_by_class_name('email').text
                break
            except:
                pass 
        self.temp_email = temp_email

    def fssp_registration(self):
        '''
        Метод позволяет вбить нужные данные на страницу регистрации ФССП
        browser: объект браузера selenium
        temp_email: str временный мейл
        company: str рандомное название компании из файла

        return: True
        '''
        TOKEN = "ecd5958c68c1c1e398e5c02a0535cdcb"
        url = 'https://rucaptcha.com/in.php'

        phone_number = '+79029' + str(random.randint(000000, 999999))
        reason_text = random.choice(['Инфоуслуги', 'Информация', 'Получение информации', 'Информационные услуги'])
        self.browser.get('https://api-ip.fssprus.ru/register')
        email = self.browser.find_element_by_id('email')
        email.send_keys(self.temp_email)
        organisation_name = self.browser.find_element_by_id('name')
        organisation_name.send_keys(self.company)
        phone = self.browser.find_element_by_id('phone')
        phone.send_keys(phone_number)
        reason = self.browser.find_element_by_id('reason')
        reason.send_keys(reason_text)
        print('CAPTCHA')
        ele_captcha = self.browser.find_element_by_xpath('//*[@id="captcha_img"]')
        img_captcha_base64 = self.browser.execute_async_script("""
        var ele = arguments[0], callback = arguments[1];
        ele.addEventListener('load', function fn(){
          ele.removeEventListener('load', fn, false);
          var cnv = document.createElement('canvas');
          cnv.width = this.width; cnv.height = this.height;
          cnv.getContext('2d').drawImage(this, 0, 0);
          callback(cnv.toDataURL('image/jpeg').substring(22));
        }, false);
        ele.dispatchEvent(new Event('load'));
        """, ele_captcha)
        with open(r"captcha.jpg", 'wb') as f:
            f.write(base64.b64decode(img_captcha_base64))
        req = requests.post(url, data={"key":TOKEN, "language":1, "json":1, "file":r'./captcha.jpg'})
        while True:
            if req.json()['status'] == 1:
                my_captcha = req.json()['request']
            elif req.json()['status'] == 0 and req.json()['request'] == 'CAPCHA_NOT_READY':
                time.sleep(5)
            else:
                print('ОШИБКА! {}. Попробуйте ввести капчу через терминал!'.format(req.json()['request']))
                captcha = self.browser.find_element_by_name('captcha')
                my_captcha = input('Введи капчу: ')
                break
        captcha.send_keys(my_captcha)
        submit_button = self.browser.find_element_by_xpath('/html/body/div[2]/section/div/div/div[2]/div/div/form/div[7]/div[1]/input')
        submit_button.click()
        self.phone_number = phone_number
        self.reason_text = reason_text
    
    def get_link(self):
        '''
        Метод получить найти ссылку на получение токена в мейле
        second_browser: объект браузера selenium (второй экземпляр)

        return: Найденная ссылка
        '''
        while True: # ждем пока придет емейл
            try:
                confirm = self.second_browser.find_element_by_xpath('/html/body/div[2]/div[5]/div/ul/li/div[1]')
                break
            except:
                pass
        message_text = confirm.text
        link = re.search(r'(https://api-ip.fssprus.ru/email/confirm/[A-Za-z0-9]+)', message_text)
        link = link.group(0)
        self.link = link

    def get_token(self):
        '''
        Метод получить найти токен на ФППС
        second_browser: объект браузера selenium
        link: str ссылка на подтверждния мейла, чтобы получить токен

        return: True - если определенное количество набралось, false - если еще надо парсить
        '''
        self.browser.get(self.link)
        token_text = self.browser.find_element_by_xpath('/html/body/div[2]/section/div/div/div[2]/div').text
        token = re.search(r'Ваш ключ доступа к API: ([A-Za-z0-9]+)', token_text)
        token = token.group(0)
        token = token.split('Ваш ключ доступа к API: ')[1]
        self.token = token
        with open('tokens.json', 'r') as f:
            tokens = json.load(f)
        tokens.append(token)
        with open('tokens.json', 'w') as f:
            json.dump(tokens, f)
        print('Осталось', self.limit-len(tokens))
        if len(tokens) >= self.limit:
            self.is_finished = True
        else:
            self.is_finished = False
        self.browser.close()
        self.second_browser.close()
